const path = require("path");
const fs = require("fs");

function readFile(path) {
  return new Promise((resolve, rejects) => {
    fs.readFile(path, "utf-8", (err, data) => {
      if (err) {
        rejects(err);
      } else {
        resolve(data);
      }
    });
  });
}


function writeFile(path, data) {
  return new Promise((resolve, rejects) => {
    fs.writeFile(path, data, (err) => {
      if (err) {
        rejects(err);
      } else {
        resolve("file created");
      }
    });
  });
}


function appendFile(path, data) {
  return new Promise((resolve, rejects) => {
    fs.appendFile(path, data, (err) => {
      if (err) {
        rejects(err);
      } else {
        resolve("file created and content added");
      }
    });
  });
}




function deleteFiles(fileNamePath){
    return new Promise((resolve, reject) =>{
        readFile(fileNamePath)
        .then((data) => {
            console.log(data);
            let newdata = data.split(" ");
            for(let index = 0; index < newdata.length;index++ ){
                fs.unlink(path.join(__dirname, newdata[index]), (err) =>{
                    if(err)
                    {
                        reject(err);
                    }
                    else
                    {
                        if(index === newdata.length -1)
                        {
                            resolve("all files deleted");

                        }
                    }

                })
            }

        })
    })
}


function func2(){
    const lipsumFilePath = __dirname + "/lipsum.txt";
    const fileNamePath = __dirname + "/filesnames.txt";
    const uppercaseFilePath = __dirname + "/uppercase.txt";
    const lowercaseFilePath = __dirname + "/lowercase.txt";
    const sortFilePath = __dirname + "/sortsentences.txt";
    readFile(lipsumFilePath)
    .then((data) => {
        console.log(data);
        return writeFile(uppercaseFilePath, data.toUpperCase())

    })
    .then((data) => {
        console.log(data)
         return appendFile(fileNamePath, "uppercase.txt")
    })
    .then((data) => {
        console.log(data);
         return readFile(uppercaseFilePath)
    })
    .then((data) => {
        console.log(data);
        data = data.toLowerCase().replaceAll(". ", "\n");
         return writeFile(lowercaseFilePath, data)
    })
    .then((data) => {
        console.log(data);
       return appendFile(fileNamePath, " lowercase.txt")
    })
    .then((data) =>{
        console.log(data);
        return readFile(lowercaseFilePath)
    })
    .then((data) =>{
        console.log(data);
        data = data.split("\n").sort().join("\n");
         return writeFile(sortFilePath, data);
    })
    .then((data) =>{
        console.log(data);
        return appendFile(fileNamePath, " sortsentences.txt")
    })
    .then((data)=>{
        console.log(data);
       return deleteFiles(fileNamePath);
    })
    .then((data) =>{
        console.log(data);
    })
    .catch((err)=>{
      console.log(err);
    })


}

// func2();
module.exports = func2;