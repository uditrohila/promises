const fs = require("fs");
const path = require("path");



// Problem 1:
    
//     Using callbacks and the fs module's asynchronous functions, do the following:
//         1. Create a directory of random JSON files
//         2. Delete those files simultaneously


// 1. first create directory then in directory create random json files and then delete it sim
//ultaneously


function createDir(dir){
    return new Promise((resolve, rejects)=> {
        fs.mkdir(dir,err=>{
            if(err)
            {
                rejects(err);
            }
            else
            {
                resolve("Dir created");
            }
        })
    })
}



function writeFile(path, data){
    return new Promise((resolve,rejects)=>{
        fs.writeFile(path,data, err=> {
            if(err){
                rejects(err);
            }
            else
            {
                resolve("file created");
            }
        })
    })
}


// function deleteFile(dir){

//     return new Promise((resolve, rejects) => {
//         // read all files from dir it give the array of all file name
//         fs.readdir(dir, (err, files) => {
//             if(err)
//             {
//                 rejects(err);
//             }

//             // iterate over the each file and send path to unlink for delete the file
//             for(const file of files){
//                 fs.unlink(path.join(dir, file), err => {
//                     if(err){
//                         rejects(err);
//                     }
//                     else
//                     {  
//                         console.log("file deleted");
//                         resolve("All the files deleted");
//                     }
//                 })
//             }
//         })
//     })

// }

function deleteFile(dir)
{
    return new Promise((resolve, rejects) => {
        // read the directory and it will give array of all files
        fs.readdir(dir, (err, files) => {
            if(err)
            {
                console.log(err);
            }

        // iterate over each file name and send the path  to the  unlink
        for(let index = 0; index < files.length; index++)
        {
            fs.unlink(path.join(dir,files[index]),err=>{
                if(err)
                {
                    console.log(err);
                }
                else
                {
                    console.log("file is deleted");
                    if(index === files.length -1)
                    {
                        resolve(" all the files is deleted promise resolved");
                    }
                }
            })
        }

            
            
        })
    })
}

function func1(){
    const dir = path.join(__dirname, 'random');
    createDir(dir)
    .then((data) =>{
        console.log(data);
      return  writeFile((path.join(dir,'file1.json')), "abcabc")
        
    }).then((data) =>{
        console.log(data);
        return writeFile((path.join(dir,'file2.json')), "abaabc")
    })
    .then((data) =>{
        console.log(data);
        return writeFile((path.join(dir,'file3.json')), "abaabc")
    })
    .then((data) =>{
        console.log(data);
        return deleteFile(dir)
    })
    .then((data) => {
        console.log(data);
    })
    .catch((err)=> {
        console.log(err);
    })
    

}


// func1()
module.exports = func1;

// remove files from the directory stackoverflow link
// https://stackoverflow.com/questions/27072866/how-to-remove-all-files-from-directory-without-removing-directory-in-node-js